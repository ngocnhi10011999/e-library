from django.contrib import admin

# Register your models here.
from . import models as md


admin.site.register(md.Category)
admin.site.register(md.Document)
admin.site.register(md.Chapter)
admin.site.register(md.Author)
admin.site.register(md.Request)
