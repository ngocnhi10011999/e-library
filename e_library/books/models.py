from datetime import datetime, timedelta

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.template.defaultfilters import slugify

from config.settings.base import AUTH_USER_MODEL

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    # products = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.slug

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)


class Author(models.Model):
    name = models.CharField(max_length=250)
    website = models.URLField(max_length=250)
    note = models.CharField(max_length=250)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Document(models.Model):
    name = models.CharField(max_length=250)
    author = models.ForeignKey(
        Author, related_name="documents", on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        Category, related_name="documents", on_delete=models.CASCADE
    )
    slug = models.SlugField(max_length=250, unique=True)
    display = models.BooleanField(default=True)
    viewed = models.IntegerField(default=0)
    image = models.ImageField()
    publishing = models.DateField()
    created_by = models.ForeignKey(
        AUTH_USER_MODEL, related_name="documents", on_delete=models.CASCADE
    )
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{0} by {1}".format(self.name, self.author)

    def __unicode__(self):
        return self.slug

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name)
        super(Document, self).save(*args, **kwargs)


class Chapter(models.Model):
    name = models.CharField(max_length=250)
    document = models.ForeignKey(
        Document, related_name="chapters", on_delete=models.CASCADE
    )
    title = models.CharField(max_length=250)
    # content = RichTextUploadingField(blank=True, null=True)
    order = models.IntegerField(default=0, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "Chapter {0} of {1}. Author: {2}".format(
            self.name, self.document, self.document.author
        )

    def __unicode__(self):
        return self.slug

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name)
        super(Chapter, self).save(*args, **kwargs)


class Request(models.Model):
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name="requests", on_delete=models.CASCADE
    )
    limited_time = models.DateField(blank=True, null=True)
    document = models.ForeignKey(
        Document, related_name="requests", on_delete=models.CASCADE
    )
    voted = models.IntegerField(
        default=0, validators=[MinValueValidator(0), MaxValueValidator(5)]
    )
    accept = models.BooleanField(default=False)
    reject = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{0} of {1}".format(self.document, self.user)

    def save(self, *args, **kwargs):
        if self.accept:
            self.limited_time = datetime.today() + timedelta(days=7)
            super(Request, self).save(*args, **kwargs)
        else:
            self.limited_time = None
            super(Request, self).save(*args, **kwargs)


class RequestDocument(models.Model):
    document = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name="request_documents", on_delete=models.CASCADE
    )
    non_exist = models.BooleanField(default=False)
    done = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.document
