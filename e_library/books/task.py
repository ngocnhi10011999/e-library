from time import sleep

from celery import shared_task
from django.core.mail import send_mail


@shared_task
def send_email_task(email, docsname, author):
    sleep(1)
    send_mail(
        "Request document fail!",
        "non exist documents {0} by {1}".format(docsname, author),
        "nnncules1001@gmail.com",
        [email],
    )
    return None
