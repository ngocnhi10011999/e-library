from datetime import date

from rest_framework import permissions

from ..models import Request


class LimitedViewPermission(permissions.BasePermission):
    """
    Global permission check for can view document.
    """

    def has_permission(self, request, view):
        try:
            if request.user.is_authenticated:
                result = Request.objects.filter(
                    user=request.user, limited_time__gte=date.today()
                )
                return result
            else:
                return False
        except Request.DoesNotExist:
            return False
