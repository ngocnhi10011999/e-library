# from datetime import date

from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, viewsets
from rest_framework.decorators import action, api_view
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.viewsets import GenericViewSet

from .. import models as md
from ..task import send_email_task
from . import serializers as srl
from .permissions import LimitedViewPermission

# from django_filters.rest_framework import DjangoFilterBackend


User = get_user_model()


class DocsFilter(filters.FilterSet):
    class Meta:
        model = md.Document
        fields = {
            "name": ["contains"],
            "viewed": ["lt"],
            "requests__voted": ["gte"],
            "author__name": ["contains"],
        }


class UserViewSet(RetrieveModelMixin, ListModelMixin, UpdateModelMixin, GenericViewSet):
    serializer_class = srl.UserSerializer
    queryset = User.objects.all()
    lookup_field = "username"

    def get_queryset(self, *args, **kwargs):
        return self.queryset.filter(id=self.request.user.id)

    @action(detail=False, methods=["GET"])
    def me(self, request):
        serializer = srl.UserSerializer(request.user, context={"request": request})
        return Response(status=status.HTTP_200_OK, data=serializer.data)

    @action(detail=False, methods=["GET"])
    def retrieve(self, request, pk=None):
        queryset = md.RequestDocument.objects.all()
        requests = get_object_or_404(queryset, pk=pk)
        serializer = srl.RequestDocsSerializer(requests, context={"request": request})
        return Response(serializer.data)


class CategoryViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = md.Category.objects.all()
        serializer = srl.CategorySerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, slug=None):
        queryset = md.Category.objects.all()
        category = get_object_or_404(queryset, slug=slug)
        serializer = srl.CategorySerializer(category, context={"request": request})
        return Response(serializer.data)

    def create(self, request):
        serializer = srl.CategorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class AuthorViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = md.Author.objects.all()
        serializer = srl.AuthorSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = md.Author.objects.all()
        author = get_object_or_404(queryset, pk=pk)
        serializer = srl.AuthorSerializer(author, context={"request": request})
        return Response(serializer.data)

    def create(self, request):
        serializer = srl.AuthorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        author = md.Author.objects.get(pk=pk)
        author.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class DocumentViewSet(viewsets.ModelViewSet):
    # queryset = md.Document.objects.all()
    # user_request = request.user.requests.filter(limited_time__gte=date.today())
    queryset = md.Document.objects.filter(display=True)
    serializer_class = srl.DocumentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_class = DocsFilter

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == "list":
            permission_classes = [LimitedViewPermission]
        elif self.action == "retrieve":
            permission_classes = [LimitedViewPermission]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    # def list(self, request):
    #     user_request = request.user.requests.filter(limited_time__gte=date.today())
    #     queryset = md.Document.objects.filter(display=True).filter(
    #         requests__in=user_request
    #     )
    #     serializer = srl.DocumentSerializer(
    #         queryset, many=True, context={"request": request}
    #     )
    #     return Response(serializer.data)

    # def retrieve(self, request, slug=None):
    #     queryset = md.Document.objects.all()
    #     document = get_object_or_404(queryset, slug=slug)
    #     serializer = srl.DocumentSerializer(document, context={"request": request})
    #     return Response(serializer.data)

    # def create(self, request):
    #     serializer = srl.DocumentSerializer(data=request.data)
    #     if serializer.is_valid():
    #         serializer.save()
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def update(self, request, pk=None):
    #     pass

    # def partial_update(self, request, pk=None):
    #     pass

    # def destroy(self, request, pk=None):
    #     pass


class ChapterViewSet(viewsets.ViewSet):

    # permission_classes = [permissions.IsAuthenticatedOrReadOnly,
    #                       LimitedViewPermission]

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action == "list":
            permission_classes = [IsAuthenticated]
        elif self.action == "retrieve":
            permission_classes = [LimitedViewPermission]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    def list(self, request):
        queryset = md.Chapter.objects.all()
        serializer = srl.ChapterSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, slug=None):
        queryset = md.Chapter.objects.all()
        chapter = get_object_or_404(queryset, slug=slug)
        serializer = srl.ChapterSerializer(chapter, context={"request": request})
        return Response(serializer.data)

    def create(self, request):
        serializer = srl.ChapterSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class SendRequestViewSet(viewsets.ViewSet):
    serializer_class = srl.SendRequestSerializer

    def list(self, request):
        queryset = md.Request.objects.all()
        serializer = srl.SendRequestSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = md.Request.objects.all()
        requests = get_object_or_404(queryset, pk=pk)
        serializer = srl.RequestSerializer(requests, context={"request": request})
        return Response(serializer.data)

    def create(self, request):
        serializer = srl.SendRequestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RequestViewSet(viewsets.ViewSet):
    queryset = md.Request.objects.all()
    serializer_class = srl.RequestSerializer

    def list(self, request):
        queryset = self.queryset
        serializer = srl.RequestSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = self.queryset
        requests = get_object_or_404(queryset, pk=pk)
        serializer = srl.RequestSerializer(requests, context={"request": request})
        return Response(serializer.data)

    def create(self, request):
        serializer = srl.RequestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        """ Updating an objects """
        queryset = md.Request.objects.all()
        requests = get_object_or_404(queryset, pk=pk)
        serializer = srl.RequestSerializer(requests, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response("Insert successfully", status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class RequestDocsViewSet(viewsets.ViewSet):
    queryset = md.RequestDocument.objects.all()
    serializer_class = srl.RequestDocsSerializer

    def list(self, request):
        queryset = md.RequestDocument.objects.all()
        serializer = srl.RequestDocsSerializer(
            queryset, many=True, context={"request": request}
        )
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = md.RequestDocument.objects.all()
        requests = get_object_or_404(queryset, pk=pk)
        serializer = srl.RequestDocsSerializer(requests, context={"request": request})
        return Response(serializer.data)

    def create(self, request):
        serializer = srl.RequestDocsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk=None):
        """ Updating an objects """
        queryset = md.RequestDocument.objects.all()
        requests = get_object_or_404(queryset, pk=pk)
        serializer = srl.RequestDocsSerializer(requests, data=request.data)
        email = requests.user.email
        docsname = requests.document
        author = requests.author
        if serializer.is_valid():
            serializer.save()
            if serializer.data["non_exist"]:
                send_email_task(email, docsname, author)
            else:
                pass
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        requestdocs = md.RequestDocument.objects.get(pk=pk)
        requestdocs.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(["GET"])
def api_root(request, format=None):
    return Response(
        {
            "documents-list": reverse(
                "books:document_list", request=request, format=format
            ),
            "author-list": reverse("books:author_list", request=request, format=format),
            "chapter-list": reverse(
                "books:chapter_list", request=request, format=format
            ),
            "category-list": reverse(
                "books:category_list", request=request, format=format
            ),
            "request-list": reverse(
                "books:request_list", request=request, format=format
            ),
            "user-list": reverse("books:user_list", request=request, format=format),
            "requestdocs-list": reverse(
                "books:requestdocs_list", request=request, format=format
            ),
        }
    )
