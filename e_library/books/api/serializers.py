from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import serializers

from .. import models as md

# from rest_framework.relations import HyperlinkedIdentityField


User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "email", "name", "url"]

        extra_kwargs = {
            "url": {"view_name": "api:user-detail", "lookup_field": "username"}
        }


class CategorySerializer(serializers.ModelSerializer):
    documents = serializers.HyperlinkedIdentityField(
        many=True,
        read_only=True,
        view_name="books:document_detail",
        lookup_field="slug",
    )

    class Meta:
        model = md.Category
        fields = ["name", "slug", "date_created", "date_updated", "documents"]


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = md.Author
        fields = ["name", "website", "note", "date_created", "date_updated", "url"]

        extra_kwargs = {
            "url": {"view_name": "books:author_detail", "lookup_field": "pk"}
        }


class DocumentSerializer(serializers.ModelSerializer):
    # chapters = serializers.HyperlinkedIdentityField(
    #     many=True, read_only=True, view_name="books:chapter_detail", lookup_field="slug"
    # )
    chapters = serializers.StringRelatedField(many=True)

    class Meta:
        model = md.Document
        fields = [
            # "url",
            "id",
            "name",
            "author",
            "category",
            "slug",
            "display",
            "viewed",
            "image",
            "publishing",
            "created_by",
            "date_created",
            "date_updated",
            "chapters",
        ]

        extra_kwargs = {
            "url": {"view_name": "books:document_detail", "lookup_field": "slug"},
        }


class ChapterSerializer(serializers.ModelSerializer):
    next_chapter = serializers.SerializerMethodField()
    previous_chapter = serializers.SerializerMethodField()

    class Meta:
        model = md.Chapter
        fields = [
            "name",
            "document",
            "title",
            "content",
            "slug",
            "next_chapter",
            "previous_chapter",
            "date_created",
            "date_updated",
            "url",
        ]

        extra_kwargs = {
            "url": {"view_name": "books:chapter_detail", "lookup_field": "slug"}
        }

    def get_next_chapter(self, obj):
        try:
            next_chapter = md.Chapter.objects.get(order=(obj.order + 1))
            return reverse("books:chapter_detail", kwargs={"slug": next_chapter.slug})
        except md.Chapter.DoesNotExist:
            return "End chapter"

    def get_previous_chapter(self, obj):
        try:
            next_chapter = md.Chapter.objects.get(order=(obj.order - 1))
            return reverse("books:chapter_detail", kwargs={"slug": next_chapter.slug})
        except md.Chapter.DoesNotExist:
            return "Begin"


class RequestSerializer(serializers.ModelSerializer):
    # url = HyperlinkedIdentityField(view_name="books:request_detail", lookup_field="pk")

    class Meta:
        model = md.Request
        fields = [
            "id",
            "user",
            "limited_time",
            "document",
            "voted",
            "accept",
            "reject",
            "date_created",
            "date_updated",
        ]


class SendRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = md.Request
        fields = [
            "document",
            "voted",
            "date_created",
            "date_updated",
        ]


class RequestDocsSerializer(serializers.ModelSerializer):
    class Meta:
        model = md.RequestDocument
        fields = [
            "id",
            "document",
            "author",
            "non_exist",
            "done",
        ]
