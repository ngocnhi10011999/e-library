from django.apps import AppConfig


class BooksConfig(AppConfig):
    name = "e_library.books"
