from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from .api import views as v

app_name = "books"

user_list = v.CategoryViewSet.as_view({"get": "list"})
user_detail = v.CategoryViewSet.as_view({"get": "retrieve"})
categories_list = v.CategoryViewSet.as_view({"get": "list", "post": "create"})
categories_detail = v.CategoryViewSet.as_view(
    {"get": "retrieve", "put": "update", "patch": "partial_update", "delete": "destroy"}
)
author_list = v.AuthorViewSet.as_view({"get": "list", "post": "create"})
author_detail = v.AuthorViewSet.as_view(
    {"get": "retrieve", "put": "update", "patch": "partial_update", "delete": "destroy"}
)
document_list = v.DocumentViewSet.as_view({"get": "list", "post": "create"})
document_detail = v.DocumentViewSet.as_view(
    {"get": "retrieve", "put": "update", "patch": "partial_update", "delete": "destroy"}
)
chapter_list = v.ChapterViewSet.as_view({"get": "list", "post": "create"})
chapter_detail = v.ChapterViewSet.as_view(
    {"get": "retrieve", "put": "update", "patch": "partial_update", "delete": "destroy"}
)
request_list = v.RequestViewSet.as_view({"get": "list", "post": "create"})
request_detail = v.RequestViewSet.as_view(
    {"get": "retrieve", "put": "update", "patch": "partial_update", "delete": "destroy"}
)
send_request = v.SendRequestViewSet.as_view({"get": "list", "post": "create"})
requestdocs_list = v.RequestDocsViewSet.as_view({"get": "list", "post": "create"})
requestdocs_detail = v.RequestDocsViewSet.as_view(
    {"get": "retrieve", "put": "update", "patch": "partial_update", "delete": "destroy"}
)

schema_view = get_schema_view(
    openapi.Info(
        title="Snippets API",
        default_version="v1",
        description="Test description",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="contact@snippets.local"),
        license=openapi.License(name="BSD License"),
    ),
    validators=["ssv", "flex"],
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=None),
        name="schema-swagger-ui",
    ),
    path(
        "redoc/", schema_view.with_ui("redoc", cache_timeout=None), name="schema-redoc"
    ),
    path("", v.api_root),
    path("user_list/", user_list, name="user_list"),
    path("user_detail/", user_detail, name="user_detail"),
    path("category_list/", categories_list, name="category_list"),
    path("category_list/<slug:slug>", categories_detail, name="category_detail"),
    path("author_list/", author_list, name="author_list"),
    path("author_list/<int:pk>", author_detail, name="author_detail"),
    path("document_list/", document_list, name="document_list"),
    path("document_list/<slug:slug>", document_detail, name="document_detail"),
    path("chapter_list/", chapter_list, name="chapter_list"),
    path("chapter_list/<slug:slug>", chapter_detail, name="chapter_detail"),
    path("request_list/", request_list, name="request_list"),
    path("send_request/", send_request, name="send_request"),
    path("request_list/<int:pk>", request_detail, name="request_detail"),
    path("requestdocs_list/", requestdocs_list, name="requestdocs_list"),
    path("requestdocs_list/<int:pk>", requestdocs_detail, name="requestdocs_detail"),
]
